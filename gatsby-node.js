const { graphql} = require("graphql")
const path = require("path")
exports.createPages = ({graphql, actions}) => {
    const {createPage} = actions;
    return graphql(`
    {
        wpcontent {
            procedures(first: 100) {
                edges {
                  node {
                    slug
                    id
                  }
                }
            }
        }
    }
    `).then(result => {
        if(result.errors){
            result.errors.forEach(e => console.error(e.toString()))
            return Promise.reject(result.errors);
        }
        const procedures = result.data.wpcontent.procedures.edges
        procedures.forEach(procedure => {
            const {id,slug} =procedure.node;
            console.log(id)
            createPage({
                path: slug,
                component: path.resolve(`src/templates/procedure.js`),
                context: {
                    id,
                    slug
                }
            })
        })
    })
}