import React, { useState, useEffect } from 'react';
import { graphql } from 'gatsby'
import { Link, useStaticQuery} from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { getUser } from '../services/auth'



function calcDistance(loc1, loc2) {
  return (loc1.longitude - loc2.longitude) + (loc2.latitude - loc2.latitude);
}

function findSchool(location, schools) {
  var closestSchool = schools[0];
  var closestDistance = 0;

  schools.map(t => {
    var distance = calcDistance(location, { latitude: t.node.organisationMeta.organisationLatitude, longitude: t.node.organisationMeta.organisationLongtitude })
    if (distance < closestDistance) {
      closestDistance = distance;
      closestSchool = t;
    }
  });

  return closestSchool.node;
}


const IndexPage = () => {
  const {
    wpcontent: {
      organisations,
      procedures
    }
  } = useStaticQuery(graphql`
    query{
      wpcontent {
        organisations {
          edges {
            node {
              title
              organisationMeta {
                organisationLatitude
                organisationLongtitude
                prevantieadviseurName
                preventieadviseurNumber
                secretariaatNumber
                directieNumber
              }
            }
          }
        }
        procedures(first: 100) {
          edges {
            node {
              id
              procedureMeta {
                title
                heading
                role {
                  name
                }
              }
              slug
            }
          }
        }
      }
    }
  `)
  const user = getUser();
  var userRole;
  if(user == null){
    userRole = "GAST"
  } else {
    userRole = user.username.roles.edges[0].node.name;
  }
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const [closeSchool, setCloseSchool] = useState()

  useEffect(() => {
    
    setCloseSchool(findSchool({latitude:51.23060, longitude:4.41377},organisations.edges));
    navigator.geolocation.getCurrentPosition((position) => {
      setLocation({ latitude: position.coords.latitude, longitude: position.coords.longitude })
    });
    
    setCloseSchool(findSchool(location,organisations.edges));
    
  },[location.latitude,location.longitude]);
  return <Layout>
    <SEO title="Home" />
    <div class="linkLijst" style={{marginTop:`90px`}}>
      {procedures.edges.map(({node: procedure}) => (
        procedure.procedureMeta.role.map((role) => (
          (role.name === userRole) &&
            <Link style={{textDecoration:'none', fontFamily:'Verdana'}}
              to={procedure.slug} 
              state={{school: closeSchool}}>
              <br/>
              <div style={{borderStyle:'solid', borderColor:'black', borderRadius: '30px' , borderWidth: '1px'  , textAlign: 'center',margin:1, padding:50, boxShadow: '5px 5px 5px grey',  textDecoration: 'none', width: 'max-width'}}>
                {procedure.procedureMeta.title}
              </div>
            </Link>
        ))
      ))}
    </div>
  </Layout>
}


export default IndexPage
