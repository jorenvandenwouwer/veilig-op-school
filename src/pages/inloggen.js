import React, {useState } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { setUser} from "../services/auth"
import { graphql } from 'gatsby'
import { useStaticQuery} from "gatsby"


const Login = () => {
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const {
        wpcontent: {
            persons
        }
    } =
        useStaticQuery(graphql`
        query{
            wpcontent {
                persons {
                    edges {
                      node {
                        roles {
                          edges {
                            node {
                              name
                            }
                          }
                        }
                        personMeta {
                          username
                          street
                          postalcode
                          phonework
                          phonehome
                          password
                          mobile
                          lastname
                          function
                          firstname
                          fieldGroupName
                          email
                          city
                        }
                      }
                    }
                  }
              }
}
`)


    function validateForm() {
        return username.length > 0 && password.length > 0;
    }
    function handleSubmit(event) {
        event.preventDefault();
        for (let index = 0; index < persons.edges.length; index++) {
            const element = persons.edges[index].node;
            if (element.personMeta.username === username && element.personMeta.password === password) {
                    
                    console.log(`${element.personMeta.username} logged in!`);
                    setUser({
                      username: element
                    })
                    window.location = "/";

            } else {
              console.log('U naam of paswoord klopt niet')
            }
        }
    }



    return (
        <Layout>
            <SEO title="Home" />
            <div style={{ marginTop: `90px` }} className="Login">
                <form onSubmit={handleSubmit}>
                    <div className="gebruikersNaam">
                        <label htmlFor="gebruikersNaam">gebruikersNaam</label>
                        <input id="gebruikersNaam" value={username} type="text" onChange={(e) => setUserName(e.target.value)}></input>
                    </div>
                    <div className="password">
                        <label htmlFor="password">paswoord</label>
                        <input id="password" value={password} type="password" onChange={(e) => setPassword(e.target.value)}></input>
                    </div>
                    <button block="true" size="lg" type="submit" disabled={!validateForm()}>
                        Login
                    </button>
                </form>
            </div>
        </Layout>
    )
}


export default Login


