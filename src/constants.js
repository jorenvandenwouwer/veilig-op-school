export const COLORS = {
    PRIMARY: "#dd882e",
    SECONDARY: "#86A92F",
    TERTIARY: "#fadcaa",
    WHITE: "#FFF",
    BLACK: "#181819",
    GREY: "#C8C8C8",
  }
  
  export const FONT_FAMILIES = {
    TITLE: "Oswald",
    TEXT: "Roboto",
    BUTTON: "Oswald",
  }
  
  export const MEDIA_QUERIES = {
    SMALL: "520px",
    MEDIUM: "980px",
    LARGE: "1200px",
  }
  