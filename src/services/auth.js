export const isBrowser = () => typeof window !== "undefined"

export const getUser = () =>
  isBrowser() && window.localStorage.getItem("gatsbyUser")
    ? JSON.parse(window.localStorage.getItem("gatsbyUser"))
    : null

export const setUser = user =>
  window.localStorage.setItem("gatsbyUser", JSON.stringify(user))

export const isLoggedIn = () => {
  const user = getUser()

  return !!user.username
}

export const getUsername = () => {
  const user = getUser();
  console.log(user)
  return user.username
  
}

export const logout = () => {
  setUser(null)
}