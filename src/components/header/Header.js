import { graphql, Link, useStaticQuery } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { HeaderWrapper, Image } from './headerStyles'
import Menu from './Menu'


const Header = ({user}) => {
  const {
    logo,
    wpcontent: {
      menuItems
    }
  } = useStaticQuery(graphql`
    query {
      logo: file(relativePath: {eq: "logo.png"}) {
        childImageSharp {
          fixed(quality: 100, width: 60 ){
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
      wpcontent {
        menuItems {
          edges {
            node {
              label
              path
            }
          }
        }
      }
    }
  `)
  return (
  <HeaderWrapper>
    <Link to="/">
      <Image alt="logo VOS" fixed={logo.childImageSharp.fixed} />
    </Link>
    <Menu user={user} menuItems={menuItems.edges} />
    
    
  </HeaderWrapper>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
