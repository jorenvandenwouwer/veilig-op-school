import React from 'react'
import {Link} from 'gatsby'
import {MenuList} from './headerStyles'
import {logout} from '../../services/auth'

const Menu = ({menuItems, user}) => {
    
    return <MenuList>
        {menuItems.map(({node: item },i) => (
            <li key={i}>
            {user == null ?
                <Link activeClassName="nav-active" to={item.path}>
                inloggen
                </Link>
                :
                <Link activeClassName="nav-active" to={item.path} onClick={logout}>
                uitloggen
                </Link>
            }
                
                
            </li>
        ))}
    </MenuList>
}

export default Menu