import React from 'react';
import {graphql} from 'gatsby';
import Layout from '../components/layout';
import SEO from "../components/seo";

const procedurePage = ({data,location}) => {
    var school = {"title":"Organisation15","organisationMeta":{"organisationLatitude":"51.2198521","organisationLongtitude":"4.480515999999966","prevantieadviseurName":"Frederic Jaminet","preventieadviseurNumber":486788723,"secretariaatNumber":486788723,"directieNumber":486788723}}
    if(location.state !== undefined){
        school = location.state.school
    }
    const procedureSteps = data.wpcontent.procedure.procedureMeta.description;
    const json = JSON.parse(JSON.parse(procedureSteps));
    const steps = json.step;
    return <Layout>
    <SEO title="Home" />
    
    <div style={{marginTop:`90px`, textAlign:'center', fontFamily:"Helvetica"}}>
    {steps.map((element) => {
        console.log(element);
        return <div style={{borderStyle:'solid', borderColor:'black', borderRadius: '30px' , borderWidth: '1px'  , textAlign: 'center'
        ,margin:10, padding:50, boxShadow: '5px 5px 5px grey',  textDecoration: 'none', width: 'max-width'}}>
            <h1>{element.title}</h1>
        {element.action !== undefined 
        && 
            element.action.map((element) => {
                let number;
                if(element.phoneNumber === "PV"){
                    number = "+32" + school.organisationMeta.preventieadviseurNumber;
                } else if(element.phoneNumber === "secretariaat"){
                    number = "+32" + school.organisationMeta.secretariaatNumber;
                } else if(element.phoneNumber === "DV"){
                    number = "+32" + school.organisationMeta.directieNumber;
                } else if(element.phoneNumber === "HD"){
                    number = "100"
                }
                if(element.code === "SMS"){
                    const text = "sms://"+ number + "?&body="+ data.wpcontent.procedure.procedureMeta.title;
                    return <div>
                        <h4>
                            {element.code} : 
                            &nbsp;<a href={text}>{element.phoneNumber}</a>&nbsp;
                            <button onClick={()=> {
                                sendsms(number, data.wpcontent.procedure.procedureMeta.title)}}
                            > automatische sms </button>
                        </h4>
                    </div>
                }else {
                    const call = "tel:" + number;
                    return <div>
                        <h4>
                            {element.code} : 
                            &nbsp;<a href={call}>{element.phoneNumber}</a>
                        </h4>
                    </div>
                }
            })
        }
        {element.list !== undefined 
        && element.list.map((element) => {
                
                    {if(element.list !== undefined){
                        return <div>
                            <b>{element.title}</b>
                            {element.list !== undefined && 
                                element.list.map((detail) => {
                                    return <li>{detail.title}</li>
                            })
                            }
                        </div>
                    
                    } else {
                        return <li>{element.title}</li>
                    }}
                    
                
            })
        }
        </div>
    }
    
    )}
    </div>  

  </Layout>
            
        
}
export const sendsms = (number, body) =>{
    console.log("sending text")
     fetch(`/.netlify/functions/sendSMS?number=${number}&body=${body}`)
     .then(res => res.json())
     .then(data => console.log(data))
}

export default procedurePage

export const pageQuery = graphql`
    query ($id:ID!) {
        
        wpcontent{
            procedure(id: $id, idType: ID) {
                procedureMeta {
                    description
                    title
                    heading
                }
                id
            }
        }
    }
    
`